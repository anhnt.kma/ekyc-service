package lfvn.vn.ekycservice.constants;

public enum VendorEnum {
	
	VNPT("vnpt"),
	GMO("gmo"),
	VIETTEL("viettel");
	
	private String vendor;
	
	private VendorEnum(String vendor) {
		this.vendor = vendor;
	}

}

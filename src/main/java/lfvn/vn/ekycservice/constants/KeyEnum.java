package lfvn.vn.ekycservice.constants;

public enum KeyEnum {
	
	TOKEN("token"),
	VENDOR_URL("vendor_url"),
	CLIENT_CODE("client-code"),
	IMAGE_BACK("image_back"),
	IMAGE_FRONT("image_front"),
	IMAGE("image"),
	IMAGE_CMT("image_cmt"),
	IMAGE_LIVE("image_live");
	
	String key;
	
	private KeyEnum(String key) {
		this.key = key;
	}
}

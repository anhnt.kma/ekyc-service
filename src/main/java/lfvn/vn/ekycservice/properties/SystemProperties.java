package lfvn.vn.ekycservice.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@ConfigurationProperties("viettel")
@Data
public class SystemProperties {
	
	private String vendorUrl;
	private String username;
	private String password;
	private String clientCode;
	
}

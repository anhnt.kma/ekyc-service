package lfvn.vn.ekycservice.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lfvn.vn.ekycservice.properties.SystemProperties;
import lombok.extern.slf4j.Slf4j;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Slf4j
@Service
public class EkycServiceImpl{
	@Autowired
	SystemProperties systemProperties;
	
	public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
	private String requestURL;
	private OkHttpClient okHttp;
	private ObjectMapper mapper = new ObjectMapper();
	private Map<String, Object> result;
	
	
	public Map<String, Object> getToken(){
		requestURL = fixUrl(systemProperties.getVendorUrl()) + "ekyc/user/authent";
		okHttp = new OkHttpClient();
		// Get new Token
		Map<String, String> requestBody = new HashMap<>();
		requestBody.put("username", systemProperties.getUsername());
		requestBody.put("password", systemProperties.getPassword());
		try {
			RequestBody requestBodyJson = RequestBody.create(mapper.writeValueAsString(requestBody), JSON);
			Request request = new Request.Builder().url(requestURL).post(requestBodyJson).build();
			try (Response response = okHttp.newCall(request).execute()) {
			      if(response.isSuccessful()) {
			    	  log.info("Response Token: " + response.body().string());
			    	  result = mapper.readValue(response.body().string(), Map.class);
			      } else throw new IOException("\"Error call API:\" + e.getMessage()");
			} catch (IOException e) {
				log.error("Error call API:" + e.getMessage());
				result.put("code", 1);
				result.put("message", "System Error");
			}
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			log.error("Parsing Json Error:" + e.getMessage());
			result.put("code", 1);
			result.put("message", "System Error");
		}
		return result;
	}

	
	public Map<String, Object> ocr(String front, String back, String token) {
		// TODO Auto-generated method stub
		requestURL = fixUrl(systemProperties.getVendorUrl()) + "ekyc/ocr/id_quality_check";
		okHttp = new OkHttpClient();
		
		Map<String, String> requestBody = new HashMap<>();
		requestBody.put("client_code", systemProperties.getClientCode());
		requestBody.put("image_back", back);
		requestBody.put("image_front", front);
		try {
			RequestBody requestBodyJson = RequestBody.create(mapper.writeValueAsString(requestBody), JSON);
			Request request = new Request.Builder().url(requestURL).addHeader("Authorization", token).post(requestBodyJson).build();
			try (Response response = okHttp.newCall(request).execute()) {
			      if(response.isSuccessful()) {
			    	  log.info("Response OCR: " + response.body().string());
			    	  result = mapper.readValue(response.body().string(), Map.class);
			      } else throw new IOException("\"Error call API:\" + e.getMessage()");
			} catch (IOException e) {
				log.error("Error call API:" + e.getMessage());
				result.put("code", 1000);
				result.put("message", "System Error");
			}
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			log.error("Parsing Json Error:" + e.getMessage());
			result.put("code", 1000);
			result.put("message", "System Error");
		}
		return result;
	}

	
	public Map<String, Object> precheckOcr(String img, String token) {
		// TODO Auto-generated method stub
		requestURL = fixUrl(systemProperties.getVendorUrl()) + "ekyc/ocr/id_quality_check";
		okHttp = new OkHttpClient();
		
		// Get new Token
		Map<String, String> requestBody = new HashMap<>();
		requestBody.put("client_code", systemProperties.getClientCode());
		requestBody.put("image", img);
		try {
			RequestBody requestBodyJson = RequestBody.create(mapper.writeValueAsString(requestBody), JSON);
			Request request = new Request.Builder().url(requestURL).addHeader("Authorization", token).post(requestBodyJson).build();
			try (Response response = okHttp.newCall(request).execute()) {
			      if(response.isSuccessful()) {
			    	  log.info("Response Precheck: " + response.body().string());
			    	  result = mapper.readValue(response.body().string(), Map.class);
			      } else throw new IOException("\"Error call API:\" + e.getMessage()");
			} catch (IOException e) {
				log.error("Error call API:" + e.getMessage());
				result.put("code", 1);
				result.put("message", "System Error");
			}
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			log.error("Parsing Json Error:" + e.getMessage());
			result.put("code", 1);
			result.put("message", "System Error");
		}
		return result;
	}

	
	public Map<String, Object> faceMatching(String front, String selfie, String token) {
		// TODO Get Data FaceMatching
		requestURL = fixUrl(systemProperties.getVendorUrl()) + "ekyc/ocr/face_matching";
		okHttp = new OkHttpClient();
		Map<String, String> requestBody = new HashMap<>();
		requestBody.put("client_code", systemProperties.getClientCode());
		requestBody.put("image_cmt", front);
		requestBody.put("image_live", selfie);
		try {
			RequestBody requestBodyJson = RequestBody.create(mapper.writeValueAsString(requestBody), JSON);
			Request request = new Request.Builder().url(requestURL).addHeader("Authorization", token).post(requestBodyJson).build();
		try (Response response = okHttp.newCall(request).execute()) {
		      if(response.isSuccessful()) {
		    	  log.info("Response Face Matching: " + response.body().string());
		    	  result = mapper.readValue(response.body().string(), Map.class);
		      } else throw new IOException("\"Error call API:\" + e.getMessage()");
		} catch (IOException e) {
			log.error("Error call API:" + e.getMessage());
		result.put("code", 1);
		result.put("message", "System Error");
			}
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
		log.error("Parsing Json Error:" + e.getMessage());
		result.put("code", 1);
		result.put("message", "System Error");
		}
		return result;
	}

	
	public Map<String, Object> liveness(String selfie, String token) {
		// TODO Auto-generated method stub
		requestURL = fixUrl(systemProperties.getVendorUrl()) + "ekyc/ocr/face_liveness";
		okHttp = new OkHttpClient();
		
		// Get new Token
		Map<String, String> requestBody = new HashMap<>();
		requestBody.put("client_code", systemProperties.getClientCode());
		requestBody.put("image", selfie);
		try {
			RequestBody requestBodyJson = RequestBody.create(mapper.writeValueAsString(requestBody), JSON);
			Request request = new Request.Builder().url(requestURL).addHeader("Authorization", token).post(requestBodyJson).build();
			try (Response response = okHttp.newCall(request).execute()) {
			      if(response.isSuccessful()) {
			    	  log.info("Response Liveness: " + response.body().string());
			    	  result = mapper.readValue(response.body().string(), Map.class);
			      } else throw new IOException("\"Error call API:\" + e.getMessage()");
			} catch (IOException e) {
				log.error("Error call API:" + e.getMessage());
				result.put("code", 1);
				result.put("message", "System Error");
			}
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			log.error("Parsing Json Error:" + e.getMessage());
			result.put("code", 1);
			result.put("message", "System Error");
		}
		return result;
	}
	
	public String fixUrl(String url) {
		if(!url.endsWith("/")) {
			url += "/";
		}
		
		return url;
	}

}

package lfvn.vn.ekycservice.service;

import java.util.Map;

public interface EkycService {
	
	Map<String, Object> getToken();
	
	Map<String, Object> ocr(String front, String back, String token);
	
	Map<String, Object> precheckOcr(String img, String token);
	
	Map<String, Object> faceMatching(String front, String selfie, String token);
	
	Map<String, Object> liveness(String selfie, String token);
}

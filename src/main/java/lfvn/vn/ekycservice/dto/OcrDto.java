package lfvn.vn.ekycservice.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OcrDto {
	private String name;
	private String idCard;
	private String birthDay;
	private String gender;
	private String address;
	private String province;
	private String district;
	private String ward;
	private String street;
	private String streetName;
	private String expiry;
	private String idType;
	
	// dan toc
	private String ethnicity;
	private String religion;
	private String issueDate;
	private String issueBy;
	private String document;

	
}

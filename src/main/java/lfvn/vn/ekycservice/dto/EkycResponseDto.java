package lfvn.vn.ekycservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EkycResponseDto {
	private String message;
	
	private Integer code;
	
	private OcrDto ocr;
}

package lfvn.vn.ekycservice.utils;

import java.io.IOException;

import org.springframework.util.Base64Utils;
import org.springframework.web.multipart.MultipartFile;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Base64EncoderUtil {
	
	public static String convertMultipartFileToBase64(MultipartFile file) {
		byte[] frontByte;
		String frontBase64 = null;
		String token;
		try {
			frontByte = file.getBytes();
			frontBase64 = Base64Utils.encodeToString(frontByte);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error("ConvertMultipartFileToBase64 Error: " + e.getMessage());;
		}
		
		return frontBase64;
	}

}

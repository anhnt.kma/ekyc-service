package lfvn.vn.ekycservice.utils;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import lfvn.vn.ekycservice.dto.OcrDto;
import lfvn.vn.ekycservice.service.EkycService;

public class ViettelAPIUtils {
	
	@Autowired
	EkycService ekycService;
	
	final int VIETTEL_TOKEN_SUCCESS_CODE = 10;
	final int VIETTEL_API_SUCCESS_CODE = 13;
	
	Integer code;
	String token;
	
	Map<String, Object> result = new HashMap<String, Object>();
	
	public Map<String, Object> ekyc(String front, String back, String selfie) {
		// check token expired from DB
		//TODO
		Map<String, Object> precheckResult = new HashMap<String, Object>();
		Map<String, Object> livenessResult = new HashMap<String, Object>();
		Map<String, Object> faceMatchingResult = new HashMap<String, Object>();
		Map<String, Object> tokenResult = new HashMap<String, Object>();
		result = ekycService.getToken();
		//Insert token into DB
		//TODO
		
		token = tokenResult.get("token") != null ? (String) tokenResult.get("token") : null;
		if(token != null) {
			precheckResult = ekycService.precheckOcr(front, token);
			code = precheckResult.get("code") != null ? (Integer) precheckResult.get("code") : null;
			if(code.equals(VIETTEL_API_SUCCESS_CODE)) {
				precheckResult = ekycService.precheckOcr(back, token);
				// Insert DB Precheck OCR
				//TODO
				code = precheckResult.get("code") != null ? (Integer) precheckResult.get("code") : null;
				if(code.equals(VIETTEL_API_SUCCESS_CODE)) {
					//liveness
					livenessResult = ekycService.liveness(selfie, token);
					code = livenessResult.get("code") != null ? (Integer) livenessResult.get("code") : null;
					if(code.equals(VIETTEL_API_SUCCESS_CODE)) {
						//FaceMatching
						faceMatchingResult = ekycService.faceMatching(front, selfie, token);
						code = faceMatchingResult.get("code") != null ? (Integer) faceMatchingResult.get("code") : null;
						if(code.equals(VIETTEL_API_SUCCESS_CODE)) {
							//OCR
							result = ekycService.ocr(front, back, token);
							code = result.get("code") != null ? (Integer) result.get("code") : null;
							if(code.equals(VIETTEL_API_SUCCESS_CODE)) {
								OcrDto ocrDto = mapValueToOcrDto(result);
								result.put("ocrDto", ocrDto);
							}
						}
					}
				}
			}
		}
		return result;
	}
	
	public OcrDto mapValueToOcrDto(Map<String, Object> data) {
		OcrDto ocrDto = new OcrDto();
		ocrDto.setIdCard(data.get("id") != null ? (String)data.get("id"): null);
		ocrDto.setName(data.get("name") != null ? (String)data.get("name"): null);
		ocrDto.setBirthDay(data.get("birthday") != null ? (String)data.get("birthday"): null);
		ocrDto.setGender(data.get("sex") != null ? (String)data.get("sex"): null);
		ocrDto.setAddress(data.get("address") != null ? (String)data.get("address"): null);
		ocrDto.setProvince(data.get("province") != null ? (String)data.get("province"): null);
		ocrDto.setDistrict(data.get("district") != null ? (String)data.get("district"): null);
		ocrDto.setStreet(data.get("street") != null ? (String)data.get("street"): null);
		ocrDto.setStreetName(data.get("street_name") != null ? (String)data.get("street_name"): null);
		ocrDto.setExpiry(data.get("expiry") != null ? (String)data.get("expiry"): null);
		ocrDto.setEthnicity(data.get("ethnicity") != null ? (String)data.get("ethnicity"): null);
		ocrDto.setReligion(data.get("religion") != null ? (String)data.get("religion"): null);
		ocrDto.setIssueDate(data.get("issue_date") != null ? (String)data.get("issue_date"): null);
		ocrDto.setIssueBy(data.get("issueBy") != null ? (String)data.get("issueBy"): null);
		ocrDto.setDocument(data.get("document") != null ? (String)data.get("document"): null);
		return ocrDto;
	}

}

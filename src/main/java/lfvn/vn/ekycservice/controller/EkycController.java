package lfvn.vn.ekycservice.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import lfvn.vn.ekycservice.dto.EkycResponseDto;
import lfvn.vn.ekycservice.dto.OcrDto;
import lfvn.vn.ekycservice.utils.Base64EncoderUtil;
import lfvn.vn.ekycservice.utils.ViettelAPIUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api")
public class EkycController {
	
	final static Integer[] SYSTEM_CODE = {
			11,13,14,15,16,20,21,101,102,103,104,105,106,107,108,110,111,112,114,116,117,118,121,122,123,
			125,126,127,128,129,130,131,132,133,136,137,138,139,140,141,142,143,144,145,135,134
	};
	
	ViettelAPIUtils viettelAPI = new ViettelAPIUtils();
	
	Integer code;
	String token;
	
	@PostMapping("/ekyc")
    public ResponseEntity<EkycResponseDto> ekycImage(@RequestParam MultipartFile front, @RequestParam MultipartFile back, @RequestParam MultipartFile selfie) {
		Map<String, Object> result = new HashMap<>();
		EkycResponseDto response = new EkycResponseDto();
		log.info("Front Encoder");
		String frontBase64 = Base64EncoderUtil.convertMultipartFileToBase64(front);
		log.info("Back Encoder");
		String backBase64 = Base64EncoderUtil.convertMultipartFileToBase64(back);
		log.info("Selfie Encoder");
		String selfieBase64 = Base64EncoderUtil.convertMultipartFileToBase64(selfie);
		
		// Get Token
		log.info("Get Token");
		result = viettelAPI.ekyc(frontBase64, backBase64, selfieBase64);
		code =  (Integer) result.get("code");
		if (Arrays.asList(SYSTEM_CODE).contains(code)) {
			response.setCode(1);
			response.setMessage("Lỗi hệ thống");
			return ResponseEntity.ok(response);
		} else {
			response.setCode(code);
			response.setMessage((String) result.get("message"));
			OcrDto ocrDto = new OcrDto();
			ocrDto = result.get("ocrDto") != null ? (OcrDto) result.get("ocrDto") : null;
			response.setOcr(ocrDto);
		}

        return ResponseEntity.ok(null);
    }

}
